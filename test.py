from pipelines.io import make_directory, make_parent_directory
from ruffus import *
from ruffus.ruffus_utility import CHECKSUM_FILE_TIMESTAMPS
from glob import *
import subprocess
from subprocess import check_output
from subprocess import Popen, PIPE, call
import argparse
import os,sys
import yaml
import time
from termcolor import colored
import functools
from pybedtools import BedTool


def run_cmd(cmd,cmd_args):
  full_cmd = str(cmd)+" "+" ".join(cmd_args)
  print full_cmd
  subprocess.call(full_cmd.split())



def filter_positions_in_amplicon(merged_positions, positions_file, result):
  with open(result+"_tmp",'w') as intersect:
    with open(merged_positions,'r') as merge_file:
      for line in merge_file :
        if len(line) > 1 :
          columns = line.strip().split("\t")
          #print columns
          outline = [chrom, position, name, ref_allele, alt_allele] = [columns[0],columns[1],columns[2],columns[3],columns[4]]
          candidate_position = position
          #print >> sys.stderr, candidate_position
          if(len(columns[3])==1 and len(columns[4])==1):
            with open(positions_file, 'r') as pos_file:
              for posline in pos_file :
                if len(posline) > 1 :
                  pos_columns = posline.strip().split("\t")
                  #print "hey"
                  pos_outline = [pos_chrom, pos_position, pos_ref_allele, pos_alt_allele, amplicon_start, amplicon_end] = [pos_columns[0],pos_columns[1],pos_columns[2],pos_columns[3],pos_columns[4], pos_columns[5].strip()]
                  if(candidate_position > amplicon_start) and (candidate_position < amplicon_end):
                    #print "found"
                    row = [chrom, candidate_position, ref_allele, alt_allele, amplicon_start, amplicon_end]
                    intersect.write("\t".join(row[i] for i in range(6)))
                    intersect.write("\n")


    with open(result,"w") as res:
      intersected = open(result+"_tmp",'r')
      intersected_tosort = csv.reader(intersected,delimiter="\t")
      intersect_sorted = sorted(intersected_tosort, key=operator.itemgetter(0))
      for line in intersect_sorted:
        res.write("\t".join(line))
        res.write("\n")

    #os.remove(result+"_tmp")






'''

def load_vcf_files():

  for root, dirs, files in os.walk("./vcfs"):
    for file in files:
        if file.endswith(".vcf"):
             print os.path.join(root, file)
             #yield [os.path.join(root, file)]

@files("./vcfs/1.vcf","./vcfs/2.vcf","./vcfs/3.vcf")
def merge_vcfs(vcf_file, merged_vcf_file):
  with open(merged_vcf_file, 'w') as ofile:
    lines = vcf_file.readlines()
    for l in lines:
      if l.startswith('#'):
        print l
        continue
      else:
        print >> ofile,l


@follows(merge_vcfs)
def end():
    pass


pipeline_run(end)



picard = "/Users/raniba/Software/picard-tools-1.113"

def _checklist(argument):
    if not type(argument) is list:
        return argument.split()
    else: return argument

def shell_out(command):
    command = _checklist(command)
    process = subprocess.Popen(command, stdout=subprocess.PIPE)
    return process.communicate()[0]

def shell_call(command):
    command = _checklist(command)
    subprocess.Popen(command, stdout=subprocess.PIPE)
    return ''


def rehead_bam_file(bam_in, bam_out):

  basename = os.path.basename(bam_in)

  sample_name = basename.split(".bam")[0]

  cmd = "java"

  cmd_args = [ "-Xmx4g -jar" , os.path.join(picard,"AddOrReplaceReadGroups.jar"),
                "INPUT="+bam_in,
                "OUTPUT="+bam_out,
                "SORT_ORDER=coordinate",
                "RGLB=8",
                "RGPL=Illumina",
                "RGPU=1",
                "RGSM="+sample_name
              ]

  run_cmd(cmd,cmd_args)
  os.remove(bam_in)


rehead_bam_file("bams/reheaded.bam", "./bams/reheaded2.bam")


jump = False

if(jump == False):
  function_name = "first_task"
else:
  function_name = "second_task"



def first_task():
    print "First task"


@follows(first_task)
def second_task():
    print "Second task"


@follows(second_task)
def final_task():
    print "Final task"


@follows(output_from(function_name))
def final_task_f():
    print "Final task"



pipeline_run()
'''
