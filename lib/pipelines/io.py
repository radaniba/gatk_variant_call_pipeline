'''
Created on 2013-09-28

@author: Andrew Roth
'''
import os
import random
import time

def make_directory(target_dir):
    '''
    Check if a directory exists and make it if not.
    
    For example, given /some/where make the folder /some/where. If /some does not exist, it will also be made.
    '''    
    i = 0
    
    while not os.path.exists(target_dir):
        # Randomly sleep for a short random time so multiple simultaneous calls don't try to create the directory. 
        time.sleep(random.random() * 10)
    
        if i > 10:
            raise Exception('Cannot create {0}'.format(target_dir))
        
        try:
            os.makedirs(target_dir)
        
        except OSError:
            i += 1

def make_parent_directory(file_name):
    '''
    Given a file name, make the parent directory if it does not exist using make_directory.
    
    For example, given /some/where/foo.bar make the folder /some/where.
    '''
    parent_dir = os.path.dirname(file_name)
    
    make_directory(parent_dir)