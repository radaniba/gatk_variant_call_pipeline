from pipelines.io import make_directory, make_parent_directory
from glob import *
import subprocess
from subprocess import check_output
from subprocess import Popen, PIPE, call
import argparse
import os,sys,yaml
import drmaa

parser = argparse.ArgumentParser(description="Rehead bam files in a bulk")


parser.add_argument('--mode', choices=['local', 'cluster', 'printout'], default='printout',
                    help='''Mode to run the pipeline in. local will run the pipeline on the compute it was launched
                    from. cluster will submit the jobs to a cluster using SGE. printout shows which tasks will be
                    run. default is printout.''')

parser.add_argument('--input_dir',
                    help='''Input is a directory containing Bam files to be checked for header''')

parser.add_argument('--output_dir',
                    help='''Output is a directory containing reheaded bam files''')

parser.add_argument('--num_cpus', type=int, default=1,
                    help='''Number of cpus to use for the analysis. If set to -1 then as many cpus as samples will
                    be used. Default is 1.''')

parser.add_argument('--config_file',
                    help='''config file''')


args = parser.parse_args()

fh = open(args.config_file)
config = yaml.load(fh)
fh.close()


picard = config['picard']
java = config['java']

make_directory(args.output_dir)



def listToStr(lst):
    """This method makes comma separated list item string"""
    return ','.join(lst)


def rehead_bam_file(bam_in, bam_out):

  basename = os.path.basename(bam_in)

  sample_name = basename.split(".bam")[0]

  cmd = java
  #cmd.replace('java','java -XX:MaxHeapSize=256m  -jar')
  cmd_args = [  "-Xmx4g", "-jar", os.path.join(picard,"AddOrReplaceReadGroups.jar"),
                "INPUT="+bam_in,
                "OUTPUT="+bam_out,
                "SORT_ORDER=coordinate",
                "RGLB=8",
                "RGPL=Illumina",
                "RGPU=1",
                "RGSM="+sample_name
              ]

  run_cmd(cmd,cmd_args,max_mem=16)


## Run reheader ###

if args.mode in ['cluster', 'local']:
    if args.mode == 'cluster':
        from pipelines.job_manager import ClusterJobManager

        import datetime

        log_dir = os.path.join(config['log_dir'], 'log', datetime.datetime.now().isoformat(','))

        job_manager = ClusterJobManager(log_dir)

    elif args.mode == 'local':
        from pipelines.job_manager import LocalJobManager

        job_manager = LocalJobManager()

    run_cmd = job_manager.run_job

    try:
        for path,subdir,files in os.walk(args.input_dir):
          for file in files :
            if file.endswith(".bam"):
              in_file = os.path.abspath(os.path.join(args.input_dir,file))
              out_file = os.path.abspath(os.path.join(args.output_dir,file))
              print "Changing Header of "+str(file)
              rehead_bam_file(in_file,out_file)
              print "done .."


    finally:
        job_manager.close()
