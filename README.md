#GATK Realignment Pipeline And Variant Calling using HaplotypeCaller

This is a python implementation of the GATK best practices guide including variant caller step. It is a functional pipeline for testing and is intended to be merged into Targeted Sequencing analysis pipeline

![pipeline](http://repo.mo.bccrc.ca/users/raniba/repos/gatk_variant_call_pipeline/browse/gatk_variant_call_pipeline.png?at=d001663f7497e2ba13acc8b913a1fba5536f03a6&raw)


The pipeline takes into account bam files (bam files directory) and outputs a bam files directory as well but realigned around target positions.
The output directory is organized like follows :

```
Output_Dir
|-- bam
|-- dedup
|-- intervals
|-- positions
|-- vcfs
`-- realigned
```

`bam` directory contains the initial input but sorted and indexed  
`dedup` directory contains the sorted bam files but dedplicated  
`intervals` directory contains target intervals created by the pipeline and around which the realignment will take place  
`realigned` is the directory containing the realigned input files
`vcfs` directory contains all the calls made on the realigned bam files
`positions` directory contains the results of looping over vcfs, merging them and outputting a single positions file that will contain variants that will be used for the counts



For more information about GATK best practices please visit https://www.broadinstitute.org/gatk/guide/best-practices


# Usage

```
python call_variants.py --mode <local/cluster/printout> \
                  --num_cpus <number_of_cpu_to_use>  \
                  --dedup <yes/no> \
                  --config_file <path_to_config_file> \
                  --input_dir <path_to_input_dir> \
                  --output_dir <path_to_output_dir> \
                  --targets <path_to_targets_file>
                  --email <valid email address>

```



#To do :

- [x]Check for the input header and rehead automatically (currently it is done manually)-
- [ ]Add options to load to database (cBioPortal)
- [x]Add option for "client" email addresse : when the pipeline is launched on the server a mail will be delivered according to a template to the user as a notification for a run in progress. Add another function to notify the user that the result is successfully done
- [x]Intersect positions file with the amplicon stars and ends, for that we need to install anther dependency pybedtools and go an IntersectBed
- [x]May be I have to revisit the switcher part, there is room for improvement by reducing the number of tasks implemented
- [ ]**Change the variant call part by a more modular design, something like call_var_gen that is a generic function that can take any external caller**
